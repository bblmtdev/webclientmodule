Sport.Exchange webclient
========================

A small library providing utility methods to execute web POST & GET requests

## Installation

  npm install https://elrombehar@bitbucket.org/bblmtdev/webclientmodule.git --save
  For Windows: npm install --save --msvs_version=2015 https://elrombehar@bitbucket.org/bblmtdev/webclientmodule.git

  run mpm addUser for authntication

  username: elrombehar
  password: **********
  email: elrom@lmtlessdev.com

## Usage

  create a webclient.config.json file on root folder:

  {
    "request" : {
      "timeout" : 5000,
  		"keepAlive" : true,
  		"keepAliveDelay" : 1000
    },
    "response" : {
      "timeout" : 5000
    },
    "global" : {
      "writeToConsole" : true
    }
  }  

  var webClient = require('sport-exchange-webclient')

  POST : webClient.postRequest(url, data, headers, function(err, returndata, responseStatusCode, statusMessage){});

  GET  : webClient.getRequest(url, headers, function(err, returndata, responseStatusCode, statusMessage){});


## Contributing

## Release History

* 1.0.0 Initial release
