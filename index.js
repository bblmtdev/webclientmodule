var Client = require('node-rest-client').Client;
var client = new Client();
var config = require('config.json')('webclient.config.json');

var writeToConsole = config.global.writeToConsole;

function getTime()
{
  var now = new Date();
  return now.toJSON();
}


function validateHeaders(headers, type)
{

  var setValue = false;

  if(type.toLowerCase() === 'post') {

   if(headers.hasOwnProperty('Content-Type')) {
     if(headers['Content-Type'] !== 'application/json') setValue = true;
   }
   else setValue = true;

   if(setValue) {
     headers['Content-Type'] = 'application/json';
      if(writeToConsole)console.log(getTime(), '[webClient] Added application/json Content-Type header');
   }
  }

   return;
}

function postRequest(reqUrl, reqData, reqHeaders, cb)
{
  if(writeToConsole)console.log(getTime(),'[webClient] Staring POST request to',reqUrl,'headers',reqHeaders,'data',reqData);

  validateHeaders(reqHeaders, 'post');

  var args = {
    data : reqData,
    headers : reqHeaders,
    requestConfig: {
		    timeout: config.request.timeout,
		    keepAlive: config.request.keepAlive,
		    keepAliveDelay: config.request.keepAliveDelay
	  },
	  responseConfig: {
		    timeout: config.response.timeout
	  }
  };

  if(writeToConsole)console.log(getTime(),'[webClient] POST request args',args);

  var req = client.post(reqUrl, args, function(returndata, response) {
    if(writeToConsole)console.log(getTime(), '[webClient] response',response,'return data',returndata);
    return cb(null, response.statusCode, returndata, response.statusMessage);
  });

  req.on('requestTimeout', function (req) {
	    req.abort();
      if(writeToConsole)console.log(getTime(),'[webClient] postRequest timeout');
      return cb(new Error('Request has timed out'),null,null);
  });

  req.on('responseTimeout', function (res) {
    if(writeToConsole)console.log(getTime(),'[webClient] postRequest response timeout');
    return cb(new Error('Response has timed out'),null,null);
  });

  req.on('error', function (err) {
    if(writeToConsole)console.log(getTime(),'[webClient] postRequest Error',err);
    return cb(err, null);
  });


}

function getRequest(reqUrl, reqHeaders, cb)
{
  if(writeToConsole)console.log(getTime(),'[webClient] Staring GET request to',reqUrl,'headers',reqHeaders);

  var args = {
    headers : reqHeaders,
    requestConfig: {
		    timeout: config.request.timeout,
		    keepAlive: config.request.keepAlive,
		    keepAliveDelay: config.request.keepAliveDelay
	  },
	  responseConfig: {
		    timeout: config.response.timeout
	  }
  };

  if(writeToConsole)console.log(getTime(),'[webClient] GET request args',args);

  var req = client.get(reqUrl, args, function(returndata, response) {
    return cb(null, response.statusCode, returndata, response.statusMessage);
  });

  req.on('requestTimeout', function (req) {
	    req.abort();
      if(writeToConsole)console.log(getTime(),'[webClient] getRequest timeout');
      return cb(new Error('Request has timed out'),null,null);
  });

  req.on('responseTimeout', function (res) {
    if(writeToConsole)console.log(getTime(),'[webClient] getRequest response timeout');
    return cb(new Error('Response has timed out'),null,null);
  });

  req.on('error', function (err) {
    if(writeToConsole)console.log(getTime(),'[webClient] getRequest Error',err);
    return cb(err, null, null);
  });
}

module.exports.postRequest = postRequest;
module.exports.getRequest = getRequest;
